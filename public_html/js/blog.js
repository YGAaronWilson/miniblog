$(function(){
    var APPLICATION_ID = "CBB67A52-4CC0-2214-FF35-157EF629B600",
        SECRET_KEY = "7DBF06C2-DDBB-495B-FF10-4EC1F29BC600" ,
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
         
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
   console.log(postsCollection);
   
   var wrapper = {
       post: postsCollection.data
   };
   
   Handlebars.registerHelper('format', function (time) {
       return moment(time).format("dddd, MMMM Do YYYY");
   });
 
 var blogScript = $("#blogs-template").html();
 var blogTemplate = Handlebars.compile(blogScript);
 var blogHTML = blogTemplate(wrapper);
 
 $('.main-coontainer').html(blogHTML);
 
 
});

function Post(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}